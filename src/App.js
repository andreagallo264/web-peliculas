import React, { Fragment, useState, useEffect } from 'react';
import Navigation from './components/Navigation';
import MoviesList from './components/MoviesList';
import axios from 'axios';

function App() {

  const [genres, setGenres] = useState([]);

  const getGenresAPI = () => {
    const url = 'https://api.themoviedb.org/3/genre/movie/list?api_key=471f9ea6134a7124d36576f28aee9a2e&language=es-AR';
    axios.get(url)
      .then(response => {
        // handle success
        setGenres(response.data.genres);
      })
      .catch(error => {
        // handle error
        console.log(error);
      })
  }

  useEffect(() => {
    getGenresAPI()
  }, [])

  return (
    <Fragment>
      <Navigation />
      <MoviesList
        genres={genres}
      />

    </Fragment>
  );
}

export default App;
