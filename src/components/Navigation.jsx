import React from 'react';
import { Navbar } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilm } from '@fortawesome/free-solid-svg-icons'

const Navigation = () => {
    return (
        <Navbar style={{ backgroundColor: '#0c0c0c' }} className="sticky-top">
            <Navbar.Brand href="#home">
                <div className='d-flex flex-column' >
                    <FontAwesomeIcon icon={faFilm} className='text-white' />
                    <FontAwesomeIcon icon={faFilm} className='text-white' />
                    <FontAwesomeIcon icon={faFilm} className='text-white' />
                    <FontAwesomeIcon icon={faFilm} className='text-white' />
                </div>
            </Navbar.Brand>
            <Navbar.Brand href="#home">
                <div>
                    <img
                        alt=""
                        src="https://fontmeme.com/permalink/200401/e208423679c78a04ecc4bbdeae16beba.png"
                        width="200"
                        height="70"
                        className="align-top"
                    />{' '}
                </div>

            </Navbar.Brand>

        </Navbar>
    );
}

export default Navigation;