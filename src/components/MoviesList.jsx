import React, { Fragment } from 'react';
import { Form } from 'react-bootstrap';


const MoviesList = ({ genres }) => {
    //https://api.themoviedb.org/3/discover/movie?api_key=471f9ea6134a7124d36576f28aee9a2e&language=es-AR&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=28%2C878
    //API para buscar peliculas por genero
    return (
        <Fragment>
            <Form>
                <Form.Group controlId="exampleForm.SelectCustom">
                    <Form.Label className='text-white'>Selecciona un género</Form.Label>
                    <Form.Control as="select" custom>
                        {genres.map(genre => (
                            <option value={genre.id} key={genre.id}>
                                {genre.name}
                            </option>
                        ))}
                    </Form.Control>
                </Form.Group>
            </Form>
        </Fragment>
    );
}

export default MoviesList;